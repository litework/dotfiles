# Dotfiles

These are my dotfiles for my arch/bspwm desktop. 

## Setup

- Application Launcher: `rofi`
- Bar: `lemonbar`
- Compositor: `compton`
- Music Player: `mpd` + `ncmpcpp`
- Shell: `zsh`
- Terminal Emulator: `urxvt`
- Text Editor: `vim`
- Video Player: `mpv`
- Web Browser: `chromium`
- Window Manager: `bspwm`
